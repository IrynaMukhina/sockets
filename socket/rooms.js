import { SECONDS_TIMER_BEFORE_START_GAME } from './config';

const rooms = [
  // { roomId: 'ROOM_1', users: [], gameStatus: false },
  // { roomId: 'ROOM_2', users: [], gameStatus: false },
  // { roomId: 'ROOM_3', users: [], gameStatus: false }
];

export default io => {
  io.on('connection', socket => {
    console.log('connected', socket.id);
    
    socket.emit('UPDATE_ROOMS', rooms);

    socket.on('JOIN_ROOM', ({ roomId, username }) => {
      const roomIndex = findRoomIndex(roomId);
      let roomUsers = rooms[roomIndex].users;
      
      if (!userExistInRoom(roomId, username)) {
        if (isAvaliablePlaceInRooms(roomId)) {
          rooms[roomIndex].users.push({name: username, status: false});
  
          roomUsers = rooms[roomIndex].users;
  
          socket.join(roomId, () => {
            io.to(socket.id).emit('JOIN_ROOM_DONE', { users: roomUsers, roomId });
          });
  
          io.to(roomId).emit('ADD_USERS_TO_LIST', { name: username, status: false });
        } else {
          io.to(socket.id).emit('NO_AVALIABLE_PLACE');
        }
      } else {
        socket.join(roomId, () => {
          io.to(socket.id).emit('JOIN_ROOM_DONE', { users: roomUsers, roomId });
        });
      }

      socket.broadcast.emit('JOIN_ROOM', { users: roomUsers, roomId });
    });

    socket.on('LEAVE_ROOM', ({ roomId, username }) => {
        const roomIndex = findRoomIndex(roomId);
        const userIndex = findUserIndex(roomId, username);

        rooms[roomIndex]?.users.splice(userIndex, 1);        

        const roomUsers = rooms[roomIndex].users;

        if(isEveryOneReady(roomId)) {
          rooms[roomIndex].gameStatus = true;

          io.to(roomId).emit('RUN_TIMER', { secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME });
        }

        if(isRoomEmpty(roomUsers)) {
          rooms.splice(roomIndex, 1);

          socket.leave(roomId, () => {
            io.to(socket.id).emit('LEAVE_ROOM_DONE', { users: roomUsers, roomId });
          });
          io.to(socket.id).emit('DELETE_ROOM', { roomId });
          socket.broadcast.emit('DELETE_ROOM', { roomId });
        } else {
          socket.leave(roomId, () => {
            io.to(socket.id).emit('LEAVE_ROOM_DONE', { users: roomUsers, roomId });
          });
  
          io.to(roomId).emit('DELETE_USERS_FROM_LIST', { name: username, status: false });
          socket.broadcast.emit('LEAVE_ROOM', { users: roomUsers, roomId });
        }
    });

    socket.on('CREATE_ROOM', (roomName, username) => {
      if (roomNameUniq(roomName)) {
        rooms.push({roomId: roomName, users: [{name: username, status: false}]});

        const roomUsers = rooms.find(room => room.roomId === roomName).users;

        socket.join(roomName, () => {
          io.to(socket.id).emit('JOIN_ROOM_DONE', { users: roomUsers, roomId: roomName, gameStatus: false });
        });

        socket.broadcast.emit('CREATE_ROOM_DONE', { users: roomUsers, roomId: roomName });
      } else {
        socket.emit('CREATE_ROOM_DONE', false);
      }
    });

    socket.on('SWITCH_USER_STATUS', ({ roomId, username }) => {
      const roomIndex = findRoomIndex(roomId);
      const userIndex = findUserIndex(roomId, username);
      console.log(rooms[roomIndex].users[userIndex]);
      
      rooms[roomIndex].users[userIndex].status = !rooms[roomIndex].users[userIndex].status;

      const userStatus = rooms[roomIndex].users[userIndex].status;

      io.to(roomId).emit('SWITCH_USER_STATUS_DONE', { name: username, status: userStatus });
      io.to(socket.id).emit('SWITCH_STATUS_DONE', { name: username, status: userStatus });

      if (isEveryOneReady(roomId)) {    
        rooms[roomIndex].gameStatus = true;
    
        io.to(roomId).emit('RUN_TIMER', { secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME });
      }
    });

    socket.on('disconnect', () => {
      console.log(`${socket.id} disconnected`);
    });
  });
};

function userExistInRoom(roomId, username) {
  const currentUsers = rooms.find(room => room.roomId === roomId).users;

  return currentUsers.findIndex(user => user.name === username) >= 0;
}

function roomNameUniq(roomName) {
  return !rooms.find(room => room.roomId === roomName);
}

function isAvaliablePlaceInRooms(roomId) {
  return rooms.find(room => room.roomId === roomId).users.length < 5;
}


function findRoomIndex(roomId) {
  return rooms.findIndex(room => room.roomId === roomId);
}

function findUserIndex(roomId, username) {
  const roomIndex = findRoomIndex(roomId);

  return rooms[roomIndex].users.findIndex(user => user.name === username);
}

function isEveryOneReady(roomId) {
  const roomUsers = rooms.find(room => room.roomId === roomId).users;

  return roomUsers.every(user => user.status) && roomUsers.length >= 2;
}

function isRoomEmpty(users) {
  return !users.length;
}