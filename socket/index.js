import * as config from './config';
import rooms from './rooms';


export default io => {
    rooms(io.of('/game'));
};
