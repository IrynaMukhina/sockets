import { createElement, addClass, removeClass, deleteElement } from './helper.mjs';

const username = sessionStorage.getItem('username');
const roomPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const roomsContainer = document.getElementById('rooms-container');
const gameInfo = document.getElementById('game-info');
const gameStatus = document.getElementById('game-status');
const addRoomButton = document.getElementById('add-room');
const leaveRoomButton = document.getElementById('leave-room');
const roomTitle = document.getElementById('room-title');

let currentRoomId = null;
let currentUser = { name: username, status: false };
let timer;

const socket = io('http://localhost:3002/game', { query: { username } });

if (!username) {
  window.location.replace('/login');
  socket.disconect();
}


const addRoom = () => {
  const roomName = prompt('Please enter room name');
  
  if (roomName.length) {
    socket.emit('CREATE_ROOM', roomName, username);
  }
}

const leaveRoom = () => {
  socket.emit('LEAVE_ROOM', { roomId: currentRoomId, username });

  currentRoomId = null;
}

addRoomButton.addEventListener('click', addRoom);
leaveRoomButton.addEventListener('click', leaveRoom);


const createRoom = ({ roomId, users }) => {
  const roomWrapper = createElement({
    tagName: 'div',
    className: `room-item__wrapper flex-centered no-select room-${roomId}`,
  });
  const usersInfo = createElement({
    tagName: 'div',
    className: 'room-item__users flex-centered no-select',
  });
  const roomName = createElement({
    tagName: 'div',
    className: 'room-item__name flex-centered no-select',
  });
  const roomButton = createElement({
    tagName: 'button',
    className: 'room-item__button',
  });

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', { roomId, username });
  };

  roomButton.addEventListener('click', onJoinRoom);

  roomName.innerText = roomId;
  roomButton.innerText = 'JOIN';
  usersInfo.innerText = `${users.length} user(s) connected`;

  roomWrapper.appendChild(usersInfo);
  roomWrapper.appendChild(roomName);
  roomWrapper.appendChild(roomButton);

  return roomWrapper;
};

const updateRooms = rooms => {
  const allRooms = rooms.map(createRoom);

  roomsContainer.innerHTML = '';
  roomsContainer.append(...allRooms);
};

const updateRoomsAfterCreate = room => {
  if (room) {
    const newRoom = createRoom(room);
  
    roomsContainer.append(newRoom);
  } else {
    alert('Room with such name have already exist');
  }
}

const showPlaceMessage = () => {
  alert('No avaliable place in this room');
}

const openGamePage = ({ users, roomId }) => {
  addClass(roomPage, 'display-none');
  removeClass(gamePage, 'display-none');

  currentRoomId = roomId;

  createGamePage({ users, roomId });
}

const createGamePage = ({ users, roomId }) => {
  roomTitle.innerHTML = roomId;
  
  createUserList(users);
  createReadyButton();
}

const createReadyButton = () => {  
  const readyButton = createElement({
    tagName: 'button',
    className: 'ready-button',
    attributes: { id: 'ready-button' }
  });

  const switchStatus = () => {
    socket.emit('SWITCH_USER_STATUS', { roomId: currentRoomId, username: currentUser.name });
  }

  readyButton.innerHTML = 'not-ready';
  readyButton.addEventListener('click', switchStatus)
  gameStatus.appendChild(readyButton);
}

const createUser = user => {    
    const userWrapper = createElement({
      tagName: 'div',
      className: 'user-wrapper',
      attributes: { id: user.name }
    });

    const userName = createElement({
        tagName: 'div',
        className: 'user-name',
    });

    const userStatus = createElement({
        tagName: 'div',
        className: `user-status user-status--${user.status ? 'ready' : 'not-ready'}`,
        attributes: { id: `${user.name}-status` }
    });    

    userName.innerText = user.name;
    userWrapper.appendChild(userName);
    userWrapper.appendChild(userStatus);

    return userWrapper;
}

const createUserList = users => {
  const allUsers = users.map(createUser);
  const usersContainer = createElement({
    tagName: 'div',
    className: 'user-container',
  })

  usersContainer.append(...allUsers);
  gameInfo.appendChild(usersContainer);
}

const backToRooms = (data) => {
  addClass(gamePage, 'display-none');
  removeClass(roomPage, 'display-none');

  currentRoomId = null;
  deleteElement('ready-button');
  deleteElement('user-container');

  if (data.users.length) {
    updateRoom(data);
  }
}


const updateRoom = ({ roomId, users }) => {  
  document.getElementById(roomId).innerHTML = `${users.length} user(s) connected`;
}

const addUsersToList = (user) => {
  const newUser = createUser(user);

  [...document.getElementsByClassName('user-container')][0].appendChild(newUser);
}

const deleteUsersFromList = (user) => {
  document.getElementById(user.name).remove();
}

const updateUserStatus = ({ name, status }) => {
  document.getElementById(`${name}-status`).classList.add(`user-status--${status ? 'ready' : 'not-ready'}`);
  document.getElementById(`${name}-status`).classList.remove(
    status ? 'user-status--not-ready' : 'user-status--ready'
  );
}

const switchStatus = ({ status }) => {  
  currentUser.status = status;
  document.getElementById('ready-button').innerHTML = status ? 'ready' : 'no-ready';
}

const hideElements = () => {
  document.getElementById('ready-button').style.display = 'none';
  document.getElementById('leave-room').style.display = 'none';
}

const showElements = () => {
  document.getElementById('ready-button').style.display = 'flex';
  document.getElementById('leave-room').style.display = 'block';
}

const runGameTimer = ({ secondsBeforeStart }) => {
  hideElements();
  const timerSeconds = createElement({
    tagName: 'div',
    className: 'timer-seconds',
    attributes: { id: 'timer-seconds' }
  });

  gameStatus.appendChild(timerSeconds);
  timerSeconds.innerHTML = secondsBeforeStart;

  timer = setInterval(() => {
    secondsBeforeStart--;
    timerSeconds.innerHTML = secondsBeforeStart

    if (secondsBeforeStart <= 0) {
      clearInterval(timer);

      document.getElementById('timer-seconds').style.display = 'none';

      showElements();
      startGame();
    }
  }, 1000);
}

const httpGet = (theUrl) => {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

const startGame = () => {
  socket.emit('START_GAME');

  const gameText = 'some text for typing';
  const timerSeconds = createElement({
    tagName: 'div',
    className: 'game-text',
    attributes: { id: 'game-text' }
  });

  timerSeconds.innerHTML = gameText;

  gameStatus.appendChild(timerSeconds);
}

const deleteRoom = ({ roomId }) => {
  deleteElement(`room-${roomId}`);
}

socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM', updateRoom);
socket.on('JOIN_ROOM_DONE', openGamePage);
socket.on('CREATE_ROOM_DONE', updateRoomsAfterCreate);
socket.on('LEAVE_ROOM_DONE', backToRooms);
socket.on('LEAVE_ROOM', updateRoom);
socket.on('ADD_USERS_TO_LIST', addUsersToList);
socket.on('DELETE_USERS_FROM_LIST', deleteUsersFromList);
socket.on('SWITCH_USER_STATUS_DONE', updateUserStatus);
socket.on('SWITCH_STATUS_DONE', switchStatus);
socket.on('RUN_TIMER', runGameTimer);
socket.on('NO_AVALIABLE_PLACE', showPlaceMessage);
socket.on('DELETE_ROOM', deleteRoom);
